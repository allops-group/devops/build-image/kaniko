FROM python:slim
ENV DEBIAN_FRONTEND noninteractive

LABEL maintainer="Aleksandar Atanasov aleksandar.vl.atanasov@gmail.com"

RUN mkdir service

COPY app/requirements.txt requirements.txt
RUN pip3 install --no-cache -r requirements.txt

RUN mkdir -p service
COPY app/* service/

WORKDIR /service/
ENTRYPOINT ["sh"]
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
